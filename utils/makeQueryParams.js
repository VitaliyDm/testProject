export default function makeQueryParams(baseUrl, params) {
  const filteredParams = Object.entries(params).reduce((acc, [key, value]) => {
    if (value) {
      acc[key] = value;
    }

    return acc;
  }, {});

  return `${baseUrl}${
    Object.keys(filteredParams).length ? "?" : ""
  }${Object.entries(filteredParams).reduce(
    (acc, [key, value], index) =>
      `${acc}${index > 0 ? "&" : ""}${key}=${value}`,
    ""
  )}`;
}
