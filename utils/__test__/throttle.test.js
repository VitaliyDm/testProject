import * as sinon from "sinon";
import throttle from "../throttle";

describe("throttle func", () => {
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers();
  });

  afterEach(() => {
    clock.restore();
  });

  test("throttle", () => {
    const func = jest.fn();
    const throttleFunc = throttle(func, 1000);

    // Call it immediately
    throttleFunc();
    expect(func).toHaveBeenCalledTimes(1); // func not called

    // Call it several times with 500ms between each call
    for (let i = 0; i < 10; i += 1) {
      clock.tick(500);
      throttleFunc();
    }
    expect(func).toHaveBeenCalledTimes(6); // func not called

    // wait 1000ms
    clock.tick(1000);
    expect(func).toHaveBeenCalledTimes(7); // func called
  });
});
