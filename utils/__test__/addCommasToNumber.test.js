import addCommasToNumber from "../addCommasToNumber";

describe("addCommasToNumber", () => {
  test("should not add comma", () => {
    expect(addCommasToNumber(10)).toBe("10");
  });

  test("should add one comma", () => {
    expect(addCommasToNumber(1000)).toBe("1,000");
  });

  test("should add two commas", () => {
    expect(addCommasToNumber(1000000)).toBe("1,000,000");
  });
});
