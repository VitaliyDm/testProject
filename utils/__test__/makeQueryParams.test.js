import makeQueryParams from "../makeQueryParams";

const baseUrl = "http://localhost:3000";

describe("makeQueryParams tests", () => {
  test("should return baseUrl", () => {
    expect(makeQueryParams(baseUrl, {})).toBe(baseUrl);
  });

  test("should add one queryParam", () => {
    expect(makeQueryParams(baseUrl, { test: "test" })).toBe(
      `${baseUrl}?test=test`
    );
  });

  test("should add more than one queryParam", () => {
    expect(makeQueryParams(baseUrl, { foo: "foo", bar: "bar" })).toBe(
      `${baseUrl}?foo=foo&bar=bar`
    );
  });
});
