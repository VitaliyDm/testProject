import prepareJobList, { sortJobs, findJobsByQuery } from "../prepareJobList";
import { sortType } from "../../constants/sortParams";

describe("sort jobs", () => {
  test("sortJobs by location ASC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                city: "NY",
              },
            ],
          },
          {
            name: "test2",
            items: [
              {
                city: "LA",
              },
            ],
          },
          {
            name: "test3",
            items: [
              {
                city: "SF",
              },
            ],
          },
        ],
        "location",
        sortType.ASC
      )
    ).toEqual([
      {
        name: "test2",
        items: [
          {
            city: "LA",
          },
        ],
      },
      {
        name: "test",
        items: [
          {
            city: "NY",
          },
        ],
      },
      {
        name: "test3",
        items: [
          {
            city: "SF",
          },
        ],
      },
    ]);
  });

  test("sort by location DESC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                city: "NY",
              },
            ],
          },
          {
            name: "test2",
            items: [
              {
                city: "LA",
              },
            ],
          },
          {
            name: "test3",
            items: [
              {
                city: "SF",
              },
            ],
          },
        ],
        "location",
        sortType.DESC
      )
    ).toEqual([
      {
        name: "test3",
        items: [
          {
            city: "SF",
          },
        ],
      },
      {
        name: "test",
        items: [
          {
            city: "NY",
          },
        ],
      },
      {
        name: "test2",
        items: [
          {
            city: "LA",
          },
        ],
      },
    ]);
  });

  test("sortJobs by role ASC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test3",
            items: [
              {
                city: "SF",
                job_title: "RN Intensive Care Unit",
              },
              {
                city: "SF",
                job_title: "Oncology Nurse",
              },
              {
                city: "SF",
                job_title: "Nursing Director",
              },
            ],
          },
        ],
        "role",
        sortType.ASC
      )
    ).toEqual([
      {
        name: "test3",
        items: [
          {
            city: "SF",
            job_title: "Nursing Director",
          },
          {
            city: "SF",
            job_title: "Oncology Nurse",
          },
          {
            city: "SF",
            job_title: "RN Intensive Care Unit",
          },
        ],
      },
    ]);
  });

  test("sortJobs by role DESC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test3",
            items: [
              {
                city: "SF",
                job_title: "RN Intensive Care Unit",
              },
              {
                city: "SF",
                job_title: "Oncology Nurse",
              },
              {
                city: "SF",
                job_title: "Nursing Director",
              },
            ],
          },
        ],
        "role",
        sortType.DESC
      )
    ).toEqual([
      {
        name: "test3",
        items: [
          {
            city: "SF",
            job_title: "RN Intensive Care Unit",
          },
          {
            city: "SF",
            job_title: "Oncology Nurse",
          },
          {
            city: "SF",
            job_title: "Nursing Director",
          },
        ],
      },
    ]);
  });

  test("sortJobs by department ASC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                department: [
                  "Pediatrics",
                  "Dental Services",
                  "Rehabilitation Services",
                  "Family Medicine",
                  "Radiation Oncology",
                ],
              },
              {
                department: [
                  "Surgery",
                  "Dental Services",
                  "Neurosurgery",
                  "Urology",
                  "Ophthalmology",
                ],
              },
            ],
          },
        ],
        "department",
        sortType.ASC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            department: [
              "Dental Services",
              "Family Medicine",
              "Pediatrics",
              "Radiation Oncology",
              "Rehabilitation Services",
            ],
          },
          {
            department: [
              "Dental Services",
              "Neurosurgery",
              "Ophthalmology",
              "Surgery",
              "Urology",
            ],
          },
        ],
      },
    ]);
  });

  test("sortJobs by department DESC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                department: [
                  "Urology",
                  "Ophthalmology",
                  "Pediatrics",
                  "Rehabilitation Services",
                  "Radiation Oncology",
                ],
              },
              {
                department: ["Surgery", "Dental Services", "Neurosurgery"],
              },
            ],
          },
        ],
        "department",
        sortType.DESC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            department: [
              "Ophthalmology",
              "Pediatrics",
              "Radiation Oncology",
              "Rehabilitation Services",
              "Urology",
            ],
          },
          {
            department: ["Dental Services", "Neurosurgery", "Surgery"],
          },
        ],
      },
    ]);
  });

  test("sortJobs by education ASC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                type: "General Acute Care",
              },
              {
                type: "Critical Access",
              },
            ],
          },
        ],
        "edu",
        sortType.ASC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            type: "Critical Access",
          },
          {
            type: "General Acute Care",
          },
        ],
      },
    ]);
  });

  test("sortJobs by education DESC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                type: "General Acute Care",
              },
              {
                type: "Critical Access",
              },
            ],
          },
        ],
        "edu",
        sortType.DESC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            type: "General Acute Care",
          },
          {
            type: "Critical Access",
          },
        ],
      },
    ]);
  });

  test("sortJobs by experience ASC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                experience: "Internship",
              },
              {
                experience: "Junior",
              },
              {
                experience: "Senior",
              },
            ],
          },
        ],
        "exp",
        sortType.ASC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            experience: "Internship",
          },
          {
            experience: "Junior",
          },
          {
            experience: "Senior",
          },
        ],
      },
    ]);
  });

  test("sortJobs by experience DESC", () => {
    expect(
      sortJobs(
        [
          {
            name: "test",
            items: [
              {
                experience: "Internship",
              },
              {
                experience: "Junior",
              },
              {
                experience: "Senior",
              },
            ],
          },
        ],
        "exp",
        sortType.DESC
      )
    ).toEqual([
      {
        name: "test",
        items: [
          {
            experience: "Senior",
          },
          {
            experience: "Junior",
          },
          {
            experience: "Internship",
          },
        ],
      },
    ]);
  });
});

describe("findJobsByQuery", () => {
  test("search by clinic name", () => {
    expect(
      findJobsByQuery(
        [
          {
            total_jobs_in_hospital: 0,
            name: "test",
            items: [],
          },
          {
            total_jobs_in_hospital: 0,
            name: "test2",
            items: [],
          },
          {
            total_jobs_in_hospital: 0,
            name: "foo",
            items: [],
          },
        ],
        "test"
      )
    ).toEqual([
      {
        total_jobs_in_hospital: 0,
        name: "test",
        items: [],
      },
      {
        total_jobs_in_hospital: 0,
        name: "test2",
        items: [],
      },
    ]);
  });

  test("search by job_title", () => {
    expect(
      findJobsByQuery(
        [
          {
            total_jobs_in_hospital: 0,
            name: "wqe",
            items: [],
            job_title: "test",
          },
          {
            total_jobs_in_hospital: 0,
            name: "qewtr",
            items: [],
            job_title: "test2",
          },
          {
            total_jobs_in_hospital: 0,
            name: "foo",
            items: [],
            job_title: "foo",
          },
        ],
        "test"
      )
    ).toEqual([
      {
        total_jobs_in_hospital: 0,
        name: "wqe",
        items: [],
        job_title: "test",
      },
      {
        total_jobs_in_hospital: 0,
        name: "qewtr",
        items: [],
        job_title: "test2",
      },
    ]);
  });

  test("search by experience", () => {
    expect(
      findJobsByQuery(
        [
          {
            total_jobs_in_hospital: 2,
            name: "test",
            items: [
              {
                experience: "Internship",
              },
              {
                experience: "Junior",
              },
            ],
            job_title: "test",
          },
          {
            total_jobs_in_hospital: 1,
            name: "test",
            items: [
              {
                experience: "Junior",
              },
            ],
            job_title: "test",
          },
        ],
        "intern"
      )
    ).toEqual([
      {
        total_jobs_in_hospital: 1,
        name: "test",
        items: [
          {
            experience: "Internship",
          },
        ],
        job_title: "test",
      },
    ]);
  });

  test("search by department", () => {
    expect(
      findJobsByQuery(
        [
          {
            total_jobs_in_hospital: 2,
            name: "test",
            items: [
              {
                experience: "Internship",
                department: ["Urology"],
              },
              {
                experience: "Junior",
                department: ["Emergency"],
              },
            ],
            job_title: "test",
          },
          {
            total_jobs_in_hospital: 1,
            name: "test2",
            items: [
              {
                experience: "Junior",
                department: ["Urology"],
              },
            ],
            job_title: "test",
          },
        ],
        "urology"
      )
    ).toEqual([
      {
        total_jobs_in_hospital: 1,
        name: "test",
        items: [
          {
            experience: "Internship",
            department: ["Urology"],
          },
        ],
        job_title: "test",
      },
      {
        total_jobs_in_hospital: 1,
        name: "test2",
        items: [
          {
            experience: "Junior",
            department: ["Urology"],
          },
        ],
        job_title: "test",
      },
    ]);
  });
});

describe("Search + sorting", () => {
  // contain right ASC order by experience
  const commonData = [
    {
      total_jobs_in_hospital: 2,
      name: "test",
      items: [
        {
          city: "SF",
          experience: "Internship",
          department: ["Urology"],
          work_schedule: "Night shift",
          created: "2020-10-24T20:04:15.502846",
          description: "text",
          salary_range: [22.22, 33.32],
          hours: [10],
          job_id: 3860,
          job_type: "Part-time",
          type: "General Acute Care",
        },
        {
          city: "SF",
          experience: "Junior",
          department: ["Emergency"],
          work_schedule: "Night shift",
          created: "2020-10-24T20:04:15.502846",
          description: "text",
          salary_range: [22.22, 33.32],
          job_type: "Part-time",
          hours: [10],
          job_id: 3860,
          type: "General Acute Care",
        },
      ],
      job_title: "test",
    },
    {
      total_jobs_in_hospital: 3,
      name: "test2",
      items: [
        {
          city: "SF",
          experience: "Internship",
          department: ["Urology"],
          work_schedule: "Night shift",
          created: "2020-10-24T20:04:15.502846",
          description: "text",
          salary_range: [22.22, 33.32],
          job_type: "Part-time",
          hours: [10],
          job_id: 3860,
          type: "General Acute Care",
        },
        {
          city: "SF",
          experience: "Junior",
          department: ["Urology"],
          work_schedule: "Night shift",
          created: "2020-10-24T20:04:15.502846",
          description: "text",
          salary_range: [22.22, 33.32],
          job_type: "Part-time",
          hours: [10],
          job_id: 3860,
          type: "General Acute Care",
        },
        {
          city: "SF",
          experience: "Senior",
          department: ["Urology"],
          work_schedule: "Night shift",
          created: "2020-10-24T20:04:15.502846",
          description: "text",
          salary_range: [22.22, 33.32],
          job_type: "Part-time",
          hours: [10],
          job_id: 3860,
          type: "General Acute Care",
        },
      ],
      job_title: "test",
    },
  ];

  test("Search by department + sort by experience", () => {
    expect(
      prepareJobList(commonData, {
        field: "exp",
        sort: sortType.ASC,
        search: "urology",
      })
    ).toEqual([
      {
        total_jobs_in_hospital: 1,
        name: "test",
        items: [
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
        ],
        job_title: "test",
      },
      {
        total_jobs_in_hospital: 3,
        name: "test2",
        items: [
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
        ],
        job_title: "test",
      },
    ]);
  });

  test("passed only search query", () => {
    expect(prepareJobList(commonData, { search: "intern" })).toEqual([
      {
        total_jobs_in_hospital: 1,
        name: "test",
        items: [
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
        ],
        job_title: "test",
      },
      {
        total_jobs_in_hospital: 1,
        name: "test2",
        items: [
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            type: "General Acute Care",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
          },
        ],
        job_title: "test",
      },
    ]);
  });

  test("passed only sort params", () => {
    expect(
      prepareJobList(
        [
          {
            total_jobs_in_hospital: 2,
            name: "test",
            items: [
              {
                city: "SF",
                experience: "Internship",
                department: ["Urology"],
                work_schedule: "Night shift",
                created: "2020-10-24T20:04:15.502846",
                description: "text",
                salary_range: [22.22, 33.32],
                hours: [10],
                job_id: 3860,
                job_type: "Part-time",
                type: "General Acute Care",
              },
              {
                city: "SF",
                experience: "Junior",
                department: ["Emergency"],
                work_schedule: "Night shift",
                created: "2020-10-24T20:04:15.502846",
                description: "text",
                salary_range: [22.22, 33.32],
                job_type: "Part-time",
                hours: [10],
                job_id: 3860,
                type: "General Acute Care",
              },
            ],
            job_title: "test",
          },
        ],
        { field: "exp", sort: sortType.ASC }
      )
    ).toEqual([
      {
        total_jobs_in_hospital: 2,
        name: "test",
        items: [
          {
            city: "SF",
            department: "Urology",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            salary_range: [22.22, 33.32],
            hours: [10],
            id: 3860,
            job_type: "Part-time",
            type: "General Acute Care",
          },
          {
            city: "SF",
            department: "Emergency",
            work_schedule: "Night shift",
            created: 22,
            summary: "text",
            salary_range: [22.22, 33.32],
            job_type: "Part-time",
            hours: [10],
            id: 3860,
            type: "General Acute Care",
          },
        ],
        job_title: "test",
      },
    ]);
  });
});
