import {
  experienceLvl,
  sortFieldToJsonField,
  sortType,
} from "../constants/sortParams";

const findJobsByQuery = (jobs, query) => {
  return jobs.reduce((acc, clinic) => {
    const newItems = (clinic.items || []).filter((job) => {
      const jobParams = Object.values(job);

      return jobParams.find((param) => {
        if (Array.isArray(param)) {
          return param.find((item) =>
            item.toString().toLowerCase().includes(query.toLowerCase())
          );
        }

        return param.toString().toLowerCase().includes(query.toLowerCase());
      });
    });

    if (
      newItems.length ||
      (clinic.name || "").toLowerCase().includes(query.toLowerCase()) ||
      (clinic.job_title || "")
        .toLowerCase()
        .toLowerCase()
        .includes(query.toLowerCase())
    ) {
      acc.push({
        ...clinic,
        total_jobs_in_hospital: newItems.length,
        items: newItems,
      });
    }

    return acc;
  }, []);
};

const getSort = (condition) => {
  if (condition) {
    return -1;
  }

  if (!condition) {
    return 1;
  }

  return 0;
};

const sortJobs = (jobs, sortField, sort) => {
  if (sortField === "location") {
    return jobs.sort((first, second) => {
      const firstLocation = first.items[0]?.city;
      const secondLocation = second.items[0]?.city;

      return sort === sortType.DESC
        ? getSort(firstLocation > secondLocation)
        : getSort(secondLocation > firstLocation);
    });
  }
  const sortedJobs = jobs.map((job) => {
    job.items.sort((first, second) => {
      let firstVal = first[sortFieldToJsonField[sortField]];
      let secondVal = second[sortFieldToJsonField[sortField]];

      if (sortField === "department") {
        firstVal = firstVal.sort()[0];
        secondVal = secondVal.sort()[0];
      }

      if (sortField === "exp") {
        return sort === sortType.DESC
          ? getSort(
              experienceLvl.indexOf(firstVal) > experienceLvl.indexOf(secondVal)
            )
          : getSort(
              experienceLvl.indexOf(firstVal) < experienceLvl.indexOf(secondVal)
            );
      }

      return sort === sortType.DESC
        ? getSort(firstVal > secondVal)
        : getSort(secondVal > firstVal);
    });

    return job;
  });

  return sortedJobs;
};

const prepareJobList = (jobs, queryParams) => {
  let result = jobs;
  if (queryParams.search) {
    result = findJobsByQuery(result, queryParams.search);
  }

  if (queryParams.sort && queryParams.field) {
    result = sortJobs(result, queryParams.field, queryParams.sort);
  }

  result = result.map((clinic) => ({
    ...clinic,
    items: clinic.items.map((job) => ({
      city: job.city,
      created: Math.round((new Date() - new Date(job.created)) / 604800000),
      salary_range: job.salary_range,
      job_type: job.job_type,
      type: job.type,
      id: job.job_id,
      department: job.department.join(", "),
      hours: job.hours,
      work_schedule: job.work_schedule,
      summary: job.description,
    })),
  }));

  return result;
};

export default prepareJobList;
export { sortJobs, getSort, findJobsByQuery };
