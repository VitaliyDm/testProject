import * as React from "react";
import PropTypes from "prop-types";

const renderNavItems = (navItems) => (
  <nav className="lg:block hidden lg:flex justify-center flex-auto">
    {navItems.map((item) => (
      <a
        href={item.link}
        key={`nav_${item.title}`}
        className="text-base font-medium text-gray-500 hover:text-gray-900 m-3"
      >
        {item.title}
      </a>
    ))}
  </nav>
);

const Header = ({ navItems, shortUserName }) => {
  return (
    <header className="relative bg-white">
      <div className="border-b-2 border-gray-100 py-6">
        <div className="max-w-7xl flex justify-between items-center mx-auto px-4 sm:px-6">
          <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-6 h-6 -mt-1 mr-2 lg:hidden"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </div>
          <div className="flex justify-start min-w-min flex-auto">
            <a href="#" className="h-8 text-blue-400 font-bold text-xl">
              HEALTH EXPLORE
            </a>
          </div>
          {renderNavItems(navItems)}
          <div className="flex lg:flex-grow-0 justify-between">
            <button
              type="button"
              className="border-blue-500 rounded text-blue-500 border p-1.5 px-2.5 hidden sm:block"
            >
              CREATE JOB
            </button>
            <div>
              <div className="rounded-full bg-blue-400 h-10 px-2.5 text-center text-white leading-9 relative ml-5 mr-5">
                {shortUserName}
                <span className="rounded-full bg-red-500 px-1.5 absolute -top-2 border-4 -right-3.5 border-white text-xs">
                  2
                </span>
              </div>
            </div>
            <button type="button" className="hidden sm:block">
              LOGOUT
            </button>
          </div>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  navItems: PropTypes.arrayOf(PropTypes.object),
  shortUserName: PropTypes.string,
};

Header.defaultProps = {
  navItems: [
    {
      title: "PROFILE",
      link: "#",
    },
    {
      title: "JOBS",
      link: "#",
    },
    {
      title: "PROFESSIONAL NETWORK",
      link: "#",
    },
    {
      title: "LOUNGE",
      link: "#",
    },
    {
      title: "SALARY",
      link: "#",
    },
  ],
  shortUserName: "JO",
};

export default Header;
