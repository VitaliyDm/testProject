import React from "react";
import PropTypes from "prop-types";

const Block = ({ children }) => (
  <section className="bg-white shadow p-4">{children}</section>
);

Block.propTypes = {
  children: PropTypes.element,
};

Block.defaultProps = {
  children: <></>,
};

export default Block;
