import React, { useEffect } from "react";
import PropTypes from "prop-types";

const Popup = ({ title, opened, onClose, children }) => {
  const handleClose = () => {
    onClose();
  };

  useEffect(() => {
    if (opened) {
      window.addEventListener("click", handleClose);
    }

    return () => {
      window.removeEventListener("click", handleClose);
    };
  }, [opened]);

  if (!opened) {
    return <></>;
  }

  return (
    <div
      className="fixed z-10 inset-0 overflow-y-auto"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div
          className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
          aria-hidden="true"
        />
        <span
          className="hidden sm:inline-block sm:align-middle sm:h-screen"
          aria-hidden="true"
        >
          &#8203;
        </span>
        <div
          className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-5xl sm:w-full"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="flex justify-between p-4 border-gray-100 border-b-2">
            <h2 className="font-medium">{title}</h2>
            <span onClick={handleClose}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="h-5 w-5 cursor-pointer"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </span>
          </div>
          <div className="p-4">{children}</div>
        </div>
      </div>
    </div>
  );
};

Popup.propTypes = {
  title: PropTypes.string,
  opened: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.element,
};

Popup.defaultProps = {
  title: "",
  opened: false,
  onClose: () => {},
  children: <></>,
};

export default Popup;
