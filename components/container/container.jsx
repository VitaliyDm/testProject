import React from "react";
import PropTypes from "prop-types";

const Container = ({ children }) => (
  <div className="bg-gray-50">
    <div className="container mx-auto max-w-7xl pd:px-4 min-h-full">
      {children}
    </div>
  </div>
);

Container.propTypes = {
  children: PropTypes.element,
};

Container.defaultProps = {
  children: <></>,
};

export default Container;
