import React from "react";

const Footer = () => {
  return (
    <footer className="grid grid-flow-row gap-4 mx-auto max-w-7xl px-4 sm:px-6 min-h-full md:flex justify-start pt-7 pb-10">
      <section className="flex-auto max-w-2xl">
        <h2 className="text-2xl font-medium mb-2">About us</h2>
        <p>
          We&nbsp;are a&nbsp;team of&nbsp;nurses, doctors, technologists and
          executives dedicated to&nbsp;help nurses find jobs that they love
        </p>
        <p className="mt-1">All copyrights reserved © 2020 - Health Explore</p>
      </section>
      <section className="lg:flex-1">
        <h2 className="text-2xl font-medium mb-2">Sitemap</h2>
        <nav className="flex flex-col">
          <a href="#" className="mb-1">
            Nurses
          </a>
          <a href="#" className="mb-1">
            Employers
          </a>
          <a href="#" className="mb-1">
            Social networking
          </a>
          <a href="#">Jobs</a>
        </nav>
      </section>
      <section className="lg:flex-1">
        <h2 className="text-2xl font-medium mb-2">Privacy</h2>
        <nav className="flex flex-col">
          <a href="#" className="mb-1">
            Terms of use
          </a>
          <a href="#" className="mb-1">
            Privacy policy
          </a>
          <a href="#" className="mb-1">
            Cookie policy
          </a>
        </nav>
      </section>
    </footer>
  );
};

export default Footer;
